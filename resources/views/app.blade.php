<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="icon" href="/img/favicon.ico" sizes="16x16" type="image/png">
    <link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="./css/main.min.css">

    <title>test messages</title>

    <meta name="keywords" content=""/>
    <meta property="og:title" content="Электронная торговая площадка Аукционы арестованного имущества Кемеровской области"/>
    <meta property="og:description" content="Электронная торговая площадка Аукционы арестованного имущества Кемеровской области"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content=""/>
</head>

<body>
    <div id="app">
        <app></app>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
