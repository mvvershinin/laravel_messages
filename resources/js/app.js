import axios from "axios";

import App from "./App";
//import VueRouter from "vue-router";
//import router from "./routes";


require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Set Vue globally
window.Vue = Vue;

//Vue.use(VueAxios, axios);

// Set Vue router
//Vue.router = router;
//Vue.use(VueRouter);

// Load Index
Vue.component("app", App);

const app = new Vue({
    el: '#app',
    //router,
});
