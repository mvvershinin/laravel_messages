<?php


namespace App\Services;


use App\Contracts\MessageStorage;

use Illuminate\Support\Facades\Storage;

class MessageStorageFile implements MessageStorage
{

    /**
     *  list of files with messages
     *
     * @return mixed
     */
    public function index()
    {
        return Storage::disk('public')->files('messages/');
    }

    /**
     * save to file
     *
     * @param $request
     * @return array
     */
    public function store($request)
    {
        $filename = 'messages/' . md5(now()) . '.json';

        Storage::disk('public')->put($filename, json_encode($request));

        return ['url' => Storage::disk('public')->url($filename)];
    }
}
