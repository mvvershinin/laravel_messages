<?php


namespace App\Services;


use App\Contracts\MessageStorage;
use App\Models\Message;

class MessageStorageEloquent implements MessageStorage
{

    /**
     *  list of messages
     *
     * @return mixed
     */
    public function index()
    {
        return Message::latest()->get(['id', 'name', 'phone', 'content']);
    }

    /**
     * save to db via eloquent
     *
     * @param $request
     * @return Message
     */
    public function store($request): Message
    {
        return Message::create($request);
    }
}
