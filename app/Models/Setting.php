<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const MESSAGES_STORE = [
        'eloquent',
        'file'
    ];

    protected $fillable = [
        'messages_store'
    ];

    /**
     * @return mixed
     */
    public static function getValue()
    {
        return Setting::firstOrCreate([])->messages_store;
    }
}
