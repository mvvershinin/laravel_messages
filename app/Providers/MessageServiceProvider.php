<?php

namespace App\Providers;

use App\Models\Setting;
use App\Services\MessageStorageEloquent;
use App\Services\MessageStorageFile;
use Illuminate\Support\ServiceProvider;

class MessageServiceProvider extends ServiceProvider
{
    private $storage;

    public function __construct($app)
    {
        parent::__construct($app);

        $this->storage =  \DB::table('settings')->first()->messages_store;
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Contracts\MessageStorage', function (){
            switch ($this->storage) {
                case 'eloquent':
                    return new MessageStorageEloquent();
                case 'file' :
                    return new MessageStorageFile();
                default:
                    return new MessageStorageFile();
            }
        });
    }
}
