<?php


namespace App\Contracts;


interface MessageStorage
{

    public function index();

    public function store($request);

}
