<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingUpdate;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        return Setting::MESSAGES_STORE;
    }

    /**
     * Display the specified resource.
     *
     * @return void
     */
    public function show()
    {
        return Setting::getValue();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function update(SettingUpdate $request, Setting $setting)
    {
        Setting::first()->update($request->toArray());
        return Setting::getValue();
    }

}
