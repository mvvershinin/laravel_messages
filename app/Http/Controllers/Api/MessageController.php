<?php

namespace App\Http\Controllers\Api;

use App\Contracts\MessageStorage;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageStore;


class MessageController extends Controller
{
    protected $messageStorage;

    /**
     * MessageController constructor.
     * @param MessageStorage $messageStorage
     */
    public function __construct(MessageStorage $messageStorage)
    {
        $this->messageStorage = $messageStorage;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->messageStorage->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MessageStore $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageStore $request)
    {
        return $this->messageStorage->store($request->toArray());
    }
}
