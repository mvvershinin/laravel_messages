<?php

namespace App\Http\Controllers\Api;

//use App\Events\Alert;
use App\Http\Requests\ChangePwdRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

use App\Models\User;

use App\Notifications\UserActivate;



use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

class AuthController extends Controller
{
    use SendsPasswordResetEmails;//, ResetsPasswords;


    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * Login user and create token
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['name', 'password']);

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 'MESSAGE_AUTH_ERROR'
            ], 401);
        }

        $user = $request->user();
        if(!$user->email_verified_at){
            return response()->json([
                'status' => 'MESSAGE_AUTH_ERROR_EMAIL_NOT_CONFIRMED'
            ], 401);
        };

        $tokenResult = $user->createToken('personal');
        $token = $tokenResult->token;

        $token->save();


        return response()->json([
            'status' => 'success',
            'data' => Auth::user()
        ], 200)->header('Authorization', $tokenResult->accessToken);
    }

    /**
     * register new user
     *
     * @param RegisterRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function register(RegisterRequest $request)
    {
        $request['password'] = Hash::make($request['password']);
        $request['role'] = 'user';


        $user = User::create($request->toArray());
        $tokenResult = $user->createToken('personal');

        $user->notify(new UserActivate($user));

        return response()->json([
                'message' => 'MESSAGE_AUTH_REGISTER',
                'status' => 'success'
            ], 200)->header('Authorization', $tokenResult->accessToken);
    }

    /**
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function verifyUser($token)
    {
        $user = User::where('token', $token)->first();

        if(isset($user) && !empty($user->token)){
            $user->markEmailAsVerified();
            //todo dont null token for test
            $user->update(['token' => null]);

            $tokenResult = $user->createToken('personal');
            $token = $tokenResult->token;
            $token->save();

            Auth::login($user);

            //return response()->json(['status' => 'success'], 200)->header('Authorization', $tokenResult->accessToken);
            return response()->json([
                'message' => 'MESSAGE_AUTH_VERIFIED',
                'status' => 'success',
                'data' => Auth::user()
            ], 200)->header('Authorization', $tokenResult->accessToken);
        }

        return response()->json(['error' => 'token not valid'], 403);
    }


    /**
     * logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->update([ 'online' => false]);
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Handle reset password
     */
    public function callResetPassword(Request $request)
    {
        return $this->reset($request);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->save();
        //event(new PasswordReset($user));
    }

    /**
     * Send password reset link.
     */
    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'success' => MESSAGE_PASSWORD_RESTORE,
        ], 200);
    }
    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['error' => 'Email could not be sent to this email address.'], 403);
    }

    /**
     * get user data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePwdRequest $request)
    {

        $user = Auth::user();
        if ( !Hash::check($request->password, $user->getAuthPassword())  ) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user->update([ 'password' => Hash::make($request->new_password) ]);

        return response()->json([
            'status' => MESSAGE_PASSWORD_UPDATED,
        ], 200);
    }


    /**
     * get user data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        return response()->json([
            'status' => 'success',
            'data' => Auth::user()//->load('profile', 'profile.documents', 'requested_lots', 'accepted_lots')
        ], 200);
    }

    public function setOnline(Request $request)
    {
        $user = Auth::user();
        $user->update(['online' => true]);
        return response()->json([
            'status' => 'user online',
        ], 200);
    }
}
