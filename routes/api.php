<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('messages', 'Api\MessageController')->only([
    'index', 'store'
]);

Route::get('/settings/list', 'Api\SettingController@index');
Route::get('/settings/get', 'Api\SettingController@show');
Route::put('/settings', 'Api\SettingController@update');
