## как развернуть

- собираем laravel


    composer install
    
- конфиг


    .env.example -> .env

    
- storage


    php artisan storage:link

- миграции


    php artisan migrate
    
- собираем фронт


    npm install
    npm run dev    

## документация Api 
<https://documenter.getpostman.com/view/1341279/SzzrXE2A?version=latest>


## Примечания

